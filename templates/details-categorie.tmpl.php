<?php
require_once("./../classes/config.inc.php");
if (empty($_SESSION['login'])) {
  require_once("./../views/connexion.php");
} else {
?>
<md-dialog aria-label="{{popup_titre_page}}">
	<form name="editionForm" ng-submit="save()">
		<md-toolbar>
			<div class="md-toolbar-tools">
				<h2>{{popup_titre_page}}</h2>
				<span flex></span>
				<md-button class="md-icon-button" ng-click="cancel()">
					<md-icon md-svg-src="images/ic_close_white_48px.svg" aria-label="Close dialog"></md-icon>
				</md-button>
			</div>
		</md-toolbar>
		<md-dialog-content layout-margin flex>
			<p>&#160;</p>
			<md-input-container class="md-block">
				<label>{{popup_nom}}</label>
				<input ng-model="categorie.nom" required>
			</md-input-container>
			<md-input-container class="md-block">
				<label>{{popup_tag}}</label>
				<input ng-model="categorie.tag" required>
			</md-input-container>
			<p>&#160;</p>
		</md-dialog-content>
		<md-dialog-actions layout="row" layout-align="end end">
			<md-button ng-click="cancel()" md-no-ink class="md-primary mdl-button-margin">{{action_annuler}}</md-button>
			<md-button type="submit" ng-disabled="editionForm.$invalid" class="md-raised md-primary mdl-button-margin">{{action_enregistrer}}</md-button>
		</md-dialog-actions>
	</form>
</md-dialog>
<?php
}
?>

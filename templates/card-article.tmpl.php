<md-card flex="80" class="md-padding alignLeft">
  <h1 class="alignCenter">{{item.nom}}</h1>
  <p class="fontBlack font08em">{{item.description}}</p>
  <p>{{item.contenu}}</p>
</md-card>

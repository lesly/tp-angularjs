<?php require_once("./../classes/config.inc.php"); ?>
<md-card>
  <md-card-header>
    <md-card-avatar>
      <md-icon class="md-avatar-icon primary" md-svg-icon="{{img_categorie}}"></md-icon>
    </md-card-avatar>
    <md-card-header-text>
      <span class="md-title">{{titre_page}}</span>
      <span class="md-subhead">{{subhead_page}}</span>
    </md-card-header-text>
  </md-card-header>
  <md-divider></md-divider>
  <div ng-show="empty" class="alignCenter">
    <h2 class="fontBlack">{{empty}}</h2>
  </div>
  <md-list-item class="list-content md-2-line" ng-repeat="item in articles | limitTo: limit as results">
    <md-menu flex="5">
      <md-button class="md-icon-button">
        <md-icon md-svg-icon="images/ic_more_vert_48px.svg" hide show-gt-sm></md-icon>
      </md-button>
      <md-menu-content width="4">
        <md-menu-item>
          <md-button ng-click="navigate('categories/' + tag_categorie + '/article-' + item.id)"> <md-icon md-svg-src="images/ic_open_in_new_48px.svg"></md-icon> <span md-menu-align-target>{{action_ouvrir}}</span> </md-button>
        </md-menu-item>
        <md-divider></md-divider>
        <md-menu-item <?php if ($_SESSION['role']=='U') { echo 'ng-hide="true"'; } ?>>
          <md-button ng-click="modifier($event, item.id)"> <md-icon md-svg-src="images/ic_mode_edit_48px.svg"></md-icon> <span md-menu-align-target>{{action_modifier}}</span> </md-button>
        </md-menu-item>
        <md-menu-item <?php if ($_SESSION['role']!='A') { echo 'ng-hide="true"'; } ?>>
          <md-button ng-click="supprimer(item.id)"> <md-icon md-svg-src="images/ic_delete_48px.svg"></md-icon> <span md-menu-align-target>{{action_supprimer}}</span> </md-button>
        </md-menu-item>
      </md-menu-content>
    </md-menu>
    <div flex class="md-list-item-text">
      <h3>{{item.nom}} <span class="font07em fontBlack">par {{item.user}}</span></h3>
      <p class="fontBlack" hide show-gt-sm>{{item.description}}</p>
    </div>
    <p flex="10" layout-align="center center"><md-button ng-click="navigate('categories/' + tag_categorie + '/article-' + item.id)" class="md-primary">{{action_ouvrir}}</md-button></p>
  </md-list-item>
  <md-list-item ng-hide="results.length >= articles.length || empty" class="noright" layout-align="center">
    <md-button class="md-block md-primary" ng-click="limit = limit + plus" aria-label="{{chargerplus}}" flex>{{chargerplus}}</md-button>
  </md-list-item>
  <md-divider></md-divider>
  <md-card-actions layout="column" layout-gt-sm="row" layout-align="end center">
    <md-button ng-click="retour()" md-no-ink class="md-primary md-button-padding">{{action_retour}}</md-button>
  </md-card-actions>
</md-card>

<md-content flex="95" align="left" class="overflow-hidden">
  <md-autocomplete md-items="item in recherche(searchText)"
                   md-search-text="searchText"
                   md-item-text="item.nom"
                   md-selected-item="carte.utilisateur"
                   md-no-cache="true"
                   md-floating-label="{{titre}}"
                   md-clear-button="true">
    <md-item-template>
      <div class="vertical-align" ng-click="navigate('categories/' + item.categorie + '/article-' + item.id)">
        <span>
          {{item.nom}} : <span class="font08em fontBlack">{{item.description | limitTo: 100}}{{item.description.length > 100 ? '...' : null}}</span>
          <span class="font07em fontBlack">(par {{item.user}})</span>
        </span>
      </div>
    </md-item-template>
    <md-not-found>
      Aucune correspondance trouvée pour "{{searchText}}".
    </md-not-found>
  </md-autocomplete>
</md-content>

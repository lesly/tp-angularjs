<md-card>
  <md-card-actions layout="column" layout-gt-sm="row" layout-align="end center">
    <md-button ng-click="retour()" md-no-ink class="md-primary md-button-padding">{{action_retour}}</md-button>
  </md-card-actions>
</md-card>

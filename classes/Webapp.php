<?php
class Webapp extends PDO {
  // Connexion localhost
  protected $db = 'tp-angularjs';
  protected $host = 'localhost';
  protected $user = 'tp-angularjs';
  protected $pwd = 'azerqsdf';
  protected $con;
  protected $select;
  protected $execute;
  protected $dns;

  public function __construct () {
    try {
      $this->con = parent::__construct($this->getDns(), $this->user, $this->pwd);
      if($this->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql')
      $this->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
      return $this->con;
    } catch(PDOException $e) {
      return date('D/m/y').' à '.date("H:i:s").' : '.$e->getMessage();
      $message= new Message();
      $message->outPut('Erreur 500', 'Serveur de BDD indisponible, nous nous excusons de la gêne occasionnée');
    }
  }

  public function select($reqSelect) {
    try {
      $this->con = parent::beginTransaction();
      $return = parent::prepare($reqSelect);
      $return->execute();
      $this->con = parent::commit();
      return $return;
    } catch (Exception $e) {
      return date('D/m/y').' à '.date("H:i:s").' : '.$e->getMessage();
      $this->con =parent::rollBack();
      $message= new Message();
      $message->outPut('Erreur dans la requêtte', 'Votre requête a été abandonné');
    }
  }

  public function count($reqSelect) {
    $result = parent::prepare($reqSelect);
    $result->execute();
    $resultat = $result->rowCount();
    return $resultat;
  }

  public function getDns() {
    return 'mysql:dbname='.$this->db.';host='.$this->host;
  }

  public function date($type = null, $data = null) {
    // Génération de la date
    $date = new DateTime();
    switch ($type) {
      case 'upload':
        $format = 'YmdHis';
        $return = $date->format($format);
        break;
      case 'texte':
        $jour_nom = strftime("%a", strtotime($data));
        $jour = date('d', strtotime($data));
        $mois = date('F', strtotime($data));
        $annee = date('Y', strtotime($data));
        switch($mois) {
          case 'January': $mois = 'jan.'; break;
          case 'February': $mois = 'fév.'; break;
          case 'March': $mois = 'mars'; break;
          case 'April': $mois = 'avr.'; break;
          case 'May': $mois = 'mai'; break;
          case 'June': $mois = 'juin'; break;
          case 'July': $mois = 'juil.'; break;
          case 'August': $mois = 'août'; break;
          case 'September': $mois = 'sept.'; break;
          case 'October': $mois = 'oct.'; break;
          case 'November': $mois = 'nov.'; break;
          case 'December': $mois = 'déc.'; break;
          default: $mois =''; break;
        }
        $return = $jour_nom.' '.$jour.' '.$mois.' '.$annee;
        break;
      case 'fr':
        $format = 'd-m-Y H:i:s';
        $return = $date->format($format);
        break;
      default:
        $format = 'Y-m-d H:i:s';
        $return = $date->format($format);
        break;
    }
    // Retourne le résultat
    return $return;
  }

  public function liste($data) {
    // Initialise le résultat à vide
    $return = '';
    // Vérifie que les données ne sont pas nulles
    if ($data['all']!=null) { $all = $data['all']; } else { $all = null; }
    if ($data['tag']!=null) { $tag = $data['tag']; } else { $tag = null; }
    // Affiche les informations du menu
    $return .= '{"nom":"Catégories","lien":"categories","image":"/images/ic_chrome_reader_mode_48px.svg"}';
    if ((isset($_SESSION['role']))&&($_SESSION['role']!="U")) { $return .= ',{"nom":"Utilisateurs","lien":"utilisateurs/liste","image":"/images/ic_person_48px.svg"}'; } // Si modérateur/administrateur
    $return ='{"reponse":['.$return.']}';
    // Retourne le résultat
    return $return;
  }

  public function aleatoire($min = null, $max = null) {
    // Retourne un int aléatoire
    return rand($min, $max);
  }

  public function email($data) {
    // Vérifie si les variables ne sont pas vides
    if ($data['nom']!=null) { $nom = $data['nom']; } else { echo '{"reponse":"Vous devez renseigner un nom !"}'; die; }
    if ($data['email']!=null) { $email = $data['email']; } else { echo '{"reponse":"L\'adresse e-mail est obligatoire."}'; die; }
    if ($data['sujet']!=null) { $sujet = $data['sujet']; } else { echo '{"reponse":"Veuillez choisir un sujet."}'; die; }
    if ($data['message']!=null) { $message = $data['message']; } else { echo '{"reponse":"Votre message est vide !"}'; die; }
    if ($data['destinataire']!=null) { $destinataire = $data['destinataire']; } else { $destinataire = 'contact@eightyfourweb.xyz'; }
    // Vérifie si les variables ne sont pas undefined
    if ($data['nom']!='undefined') { $nom = $data['nom']; } else { echo '{"reponse":"Vous devez renseigner un nom !"}'; die; }
    if ($data['email']!='undefined') { $email = $data['email']; } else { echo '{"reponse":"L\'adresse e-mail est obligatoire."}'; die; }
    if ($data['sujet']!='undefined') { $sujet = $data['sujet']; } else { echo '{"reponse":"Votre message est vide !"}'; die; }
    if ($data['message']!='undefined') { $message = $data['message']; } else { echo '{"reponse":"Votre message est vide !"}'; die; }
    if ($data['destinataire']!='undefined') { $destinataire = $data['destinataire']; } else { $destinataire = 'contact@eightyfourweb.xyz'; }
    $headers = 'From: '.$email."\r\n".
        'Reply-To: '.$email."\r\n".
        'X-Mailer: PHP/' . phpversion();
    mail($destinataire, $sujet, $message, $headers);
    return '{"reponse":"correct"}';
  }

}
?>

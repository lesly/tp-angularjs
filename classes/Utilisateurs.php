<?php
class Utilisateurs extends Webapp {

  public function ajouter($data) {
    $return = '';
    // Vérifie que les données sont non vides !
    if (!empty($data['login'])) { $login = utf8_encode($data['login']); } else { echo '{"reponse":"donnee est vide."}'; die; }
    if (!empty($data['pass'])) { $pass = base64_encode($data['pass']); } else { echo '{"reponse":"donnee est vide."}'; die; }
    if (!empty($data['nom'])) { $nom = utf8_encode($data['nom']); } else { echo '{"reponse":"donnee est vide."}'; die; }
    if (!empty($data['prenom'])) { $prenom = utf8_encode($data['prenom']); } else { echo '{"reponse":"donnee est vide."}'; die; }
    if (!empty($data['email'])) { $email = $data['email']; } else { echo '{"reponse":"donnee est vide."}'; die; }
    try {
      $this->con = parent::beginTransaction();
      $result = parent::prepare('INSERT INTO `t_users` (`id_user`, `actif_user`, `login_user`, `pass_user`, `email_user`, `nom_user`, `prenom_user`, `role_user`) VALUES (NULL, 1, :login, :pass, :email, :nom, :prenom, "U");');
      $result->bindParam(':login', $login, PDO::PARAM_STR);
      $result->bindParam(':pass', $pass, PDO::PARAM_STR);
      $result->bindParam(':email', $email, PDO::PARAM_STR);
      $result->bindParam(':nom', $nom, PDO::PARAM_STR);
      $result->bindParam(':prenom', $prenom, PDO::PARAM_STR);
      $result->execute();
      $this->con = parent::commit();
      $return = '{"reponse":"correct"}';
    } catch (Exception $e) {
      $return = '{"reponse":"La requête MySQL a été abandonné."}';
    }
    return $return;
  }

  public function connexion($data) {
    // Initialise le résultat à vide
    $return = '';
    // Vérifie que les données ne sont pas nulles
    if ($data['login']!=null) { $login = $data['login']; } else { echo '{"reponse":[{"message":"Le nom d\'utilisateur est vide !"}]}'; die; }
    if ($data['pass']!=null) { $pass = base64_encode($data['pass']); } else { echo '{"reponse":[{"message":"Le mot de passe est vide !"}]}'; die; }
    // Requête MySQL
    $requete = self::select('SELECT login_user, pass_user, nom_user, prenom_user, role_user FROM t_users WHERE login_user = "'.$login.'" AND pass_user = "'.$pass.'" AND actif_user = 1;');
    // Récupération des données
    foreach ($requete as $row) {
      // Vérifie que les données envoyées correspondent
      if (($row['login_user']==$login)&&($row['pass_user']==$pass)) {
        // Sauvegarde les variables de sessions
        $_SESSION['login'] = utf8_encode($row['login_user']);
        $_SESSION['nom'] = utf8_encode($row['nom_user']);
        $_SESSION['prenom'] = utf8_encode($row['prenom_user']);
        $_SESSION['role'] = utf8_encode($row['role_user']);
      } else {
        // L'utilisateur n'existe pas !
        echo '{"reponse":"Le nom d\'utilisateur et le mot de passe ne correspondent pas !"}'; die;
      }
    }
    // Insertion du résultat dans un tableau JSON
    $return ='{"reponse":"correct"}';
    // Retourne le résultat
    return $return;
  }

  public function deconnexion() {
    // Définit toutes les variables de sessions à null
    $_SESSION['login'] = null;
    $_SESSION['nom'] = null;
    $_SESSION['prenom'] = null;
    $_SESSION['role'] = null;
    // Vide et détruit la session
    session_unset();
    session_destroy();
    // Vérifie que la session est bien vide
    if(empty($_SESSION['login'])) {
      $result = '{"reponse":"correct"}';
    } else {
      $result = '{"reponse":"Une erreur a été rencontré lors de la déconnexion. Merci de recommencer."}';
    }
    // Retourne le résultat
    return $result;
  }

  public function liste($data) {
    // Initialise le résultat à vide
    $return = '';
    // Vérifie que les données ne sont pas nulles
    if ($data['all']==false) { $all = 'WHERE role_user != "A" AND role_user != "M"'; } else { $all = ''; }
    if ($data['tag']!=null) { $tag = $data['tag']; } else { $tag = null; }
    // Requête MySQL
    $requete = self::select('SELECT * FROM t_users '.$all.';');
    // Récupération des données
    foreach ($requete as $row) {
      // Si le résultat n'est pas vide, on ajoute une virgule pour la suite du tableau
      if ($return!="") { $return .= ","; }
      // Définit un nom aux rôles utilisateurs BDD
      switch ($row['role_user']) {
        case 'A':
          $role_user = 'Administrateur';
          break;
        case 'M':
          $role_user = 'Modérateur';
          break;
        default:
          $role_user = 'Utilisateur';
          break;
      }
      $return .= '{';
      $return .= '"id":"'.$row['id_user'].'",';
      $return .= '"actif":"'.$row['actif_user'].'",';
      $return .= '"login":"'.utf8_encode($row['login_user']).'",';
      $return .= '"pass":"'.base64_decode($row['pass_user']).'",';
      $return .= '"email":"'.utf8_encode($row['email_user']).'",';
      $return .= '"nom":"'.utf8_encode($row['nom_user']).'",';
      $return .= '"prenom":"'.utf8_encode($row['prenom_user']).'",';
      $return .= '"role":"'.$role_user.'"';
      $return .= '}';
    }
    // Insertion du résultat dans un tableau JSON
    $return ='{"reponse":['.$return.']}';
    // Retourne le résultat
    return $return;
  }

}
?>

<?php
// Démarrage de la session PHP
session_start();
// Auto-loader des classes
spl_autoload_register(function ($class) {
	include '../classes/' . $class . '.php';
});
?>

<?php
class Categories extends Webapp {

  public function afficher($data) {
    // Initialise le résultat à vide
    $return = '';
    // Vérifie que les données ne sont pas nulles
    if ($data['tag']!=null) { $tag = $data['tag']; } else { echo '{"reponse":"error"}'; die; }
    if ($data['id']!=null) { $id = $data['id']; } else { $id = null; }
    // Requête MySQL
    $requete = self::select('SELECT * FROM t_categories WHERE tag_categorie = "'.$tag.'";');
    // Récupération des données
    foreach ($requete as $row) {
      // Si le résultat n'est pas vide, on ajoute une virgule pour la suite du tableau
      if ($return!="") { $return .= ","; }
      $return .= '{';
      $return .= '"id":"'.$row['id_categorie'].'",';
      $return .= '"image":"images/ic_cat_'.utf8_encode($row['tag_categorie']).'.svg",';
      $return .= '"tag":"'.utf8_encode($row['tag_categorie']).'",';
      $return .= '"nom":"'.utf8_encode($row['nom_categorie']).'"';
      $return .= '}';
    }
    // Insertion du résultat dans un tableau JSON
    $return ='{"reponse":['.$return.']}';
    // Retourne le résultat
    return $return;
  }

  public function liste($data) {
    // Initialise le résultat à vide
    $return = '';
    // Vérifie que les données ne sont pas nulles
    if ($data['all']==false) { $all = ''; } else { $all = ''; }
    // Empêche le traitement des données non désirées
    if ($data['tag']!=null) { $tag = null; } else { $tag = null; }
    // Requête MySQL
    $requete = self::select('SELECT id_categorie, tag_categorie, nom_categorie FROM t_categories '.$all.';');
    // Récupération des données
    foreach ($requete as $row) {
      // Si le résultat n'est pas vide, on ajoute une virgule pour la suite du tableau
      if ($return!="") { $return .= ","; }
      $return .= '{';
      $return .= '"id":"'.$row['id_categorie'].'",';
      $return .= '"image":"images/ic_cat_'.utf8_encode($row['tag_categorie']).'.svg",';
      $return .= '"tag":"'.utf8_encode($row['tag_categorie']).'",';
      $return .= '"nom":"'.utf8_encode($row['nom_categorie']).'"';
      $return .= '}';
    }
    // Insertion du résultat dans un tableau JSON
    $return ='{"reponse":['.$return.']}';
    // Retourne le résultat
    return $return;
  }

	public function editer($data) {
		$return = '';
		// Vérifie que les données sont non vides !
    if (!empty($data['id'])) { $id = utf8_encode($data['id']); } else { echo '{"reponse":"La catégorie doit posséder un ID."}'; die; }
    if (!empty($data['nom'])) { $nom = utf8_encode($data['nom']); } else { echo '{"reponse":"Vous devez spécifier un nom."}'; die; }
		if (!empty($data['tag'])) { $tag = utf8_encode($data['tag']); } else { echo '{"reponse":"Vous devez spécifier un mot-clé."}'; die; }
		try {
			$this->con = parent::beginTransaction();
			$result = parent::prepare('UPDATE `t_categories` SET `nom_categorie` = :nom, `tag_categorie` = :tag WHERE `t_categories`.`id_categorie` = :id;');
			$result->bindParam(':id', $id, PDO::PARAM_STR);
      $result->bindParam(':nom', $nom, PDO::PARAM_STR);
      $result->bindParam(':tag', $tag, PDO::PARAM_STR);
			$result->execute();
			$this->con = parent::commit();
			$return = '{"reponse":"correct"}';
		} catch (Exception $e) {
			$return = '{"reponse":"La requête MySQL a été abandonné."}';
		}
		return $return;
	}

}
?>

<?php
class Articles extends Webapp {

  public function afficher($data) {
    // Initialise le résultat à vide
    $return = '';
    // Vérifie que les données ne sont pas nulles
    if ($data['tag']!=null) { $tag = $data['tag']; } else { $tag = null; }
    if ($data['id']!=null) { $id = $data['id']; } else { echo '{"reponse":"error"}'; die; }
    // Requête MySQL
    $requete = self::select('SELECT * FROM t_articles WHERE id_article = "'.$id.'";');
    // Récupération des données
    foreach ($requete as $row) {
      // Si le résultat n'est pas vide, on ajoute une virgule pour la suite du tableau
      if ($return!="") { $return .= ","; }
      $return .= '{';
      $return .= '"id":"'.$row['id_article'].'",';
      // Chargement des infos de la catégorie
      $requete1 = self::select('SELECT tag_categorie, nom_categorie FROM t_categories WHERE id_categorie = "'.$row['id_categorie'].'";');
      // Récupération des données
      foreach ($requete1 as $row1) {
        $return .= '"tag_categorie":"'.utf8_encode($row1['tag_categorie']).'",';
        $return .= '"nom_categorie":"'.utf8_encode($row1['nom_categorie']).'",';
      }
      // Chargement des infos utilisateur
      $requete2 = self::select('SELECT * FROM t_users WHERE id_user = "'.$row['id_user'].'";');
      // Récupération des données
      foreach ($requete2 as $row2) {
        $user = $row2['prenom_user'].' '.$row2['nom_user'];
        $return .= '"user":"'.utf8_encode($user).'",';
      }
      $return .= '"tag":"'.utf8_encode($row['tag_article']).'",';
      $return .= '"nom":"'.utf8_encode($row['nom_article']).'",';
      $return .= '"description":"'.utf8_encode($row['description_article']).'",';
      $return .= '"contenu":"'.utf8_encode($row['contenu_article']).'"';
      $return .= '}';
    }
    // Insertion du résultat dans un tableau JSON
    $return ='{"reponse":['.$return.']}';
    // Retourne le résultat
    return $return;
  }

  public function liste($data) {
    // Initialise le résultat à vide
    $return = '';
    // Vérifie que les données ne sont pas nulles
    if ($data['tag']!=null) { $tag = 'WHERE id_categorie IN (SELECT id_categorie FROM t_categories WHERE tag_categorie = "'.$data['tag'].'")'; } else { $tag = ''; }
    // Empêche le traitement des données non désirées
    if ($data['all']!=null) { $all = null; } else { $all = null; }
    // Requête MySQL
    $requete = self::select('SELECT id_article, id_user, tag_article, nom_article, description_article FROM t_articles '.$tag.';');
    // Récupération des données
    foreach ($requete as $row) {
      // Si le résultat n'est pas vide, on ajoute une virgule pour la suite du tableau
      if ($return!="") { $return .= ","; }
      $return .= '{';
      $return .= '"id":"'.utf8_encode($row['id_article']).'",';
      // Chargement des infos utilisateur
      $requete2 = self::select('SELECT * FROM t_users WHERE id_user = "'.$row['id_user'].'";');
      // Récupération des données
      foreach ($requete2 as $row2) {
        $user = $row2['prenom_user'].' '.$row2['nom_user'];
        $return .= '"user":"'.utf8_encode($user).'",';
      }
      $return .= '"tag":"'.utf8_encode($row['tag_article']).'",';
      $return .= '"nom":"'.utf8_encode($row['nom_article']).'",';
      $return .= '"description":"'.utf8_encode($row['description_article']).'"';
      $return .= '}';
    }
    // Insertion du résultat dans un tableau JSON
    $return ='{"reponse":['.$return.']}';
    // Retourne le résultat
    return $return;
  }

  public function recherche($data) {
    // Initialise le résultat à vide
    $return = '';
    // Vérifie que les données ne sont pas nulles
    if (!empty($data['q'])) { $q = $data['q']; } else { $q = null; }
    // Requête MySQL
    $requete = self::select('SELECT id_article, id_user, id_categorie, tag_article, nom_article, description_article FROM t_articles WHERE tag_article LIKE "%'.$q.'%" OR nom_article LIKE "%'.$q.'%" OR description_article LIKE "%'.$q.'%" OR contenu_article LIKE "%'.$q.'%";');
    // Récupération des données
    foreach ($requete as $row) {
      // Si le résultat n'est pas vide, on ajoute une virgule pour la suite du tableau
      if ($return!="") { $return .= ","; }
      $return .= '{';
      $return .= '"id":"'.utf8_encode($row['id_article']).'",';
      // Chargement des infos utilisateur
      $requete2 = self::select('SELECT prenom_user, nom_user FROM t_users WHERE id_user = "'.$row['id_user'].'";');
      // Récupération des données
      foreach ($requete2 as $row2) {
        $user = $row2['prenom_user'].' '.$row2['nom_user'];
        $return .= '"user":"'.utf8_encode($user).'",';
      }
      // Chargement des infos de la categorie
      $requete3 = self::select('SELECT tag_categorie FROM t_categories WHERE id_categorie = "'.$row['id_categorie'].'";');
      // Récupération des données
      foreach ($requete3 as $row3) {
        $return .= '"categorie":"'.utf8_encode($row3['tag_categorie']).'",';
      }
      $return .= '"tag":"'.utf8_encode($row['tag_article']).'",';
      $return .= '"nom":"'.utf8_encode($row['nom_article']).'",';
      $return .= '"description":"'.utf8_encode($row['description_article']).'"';
      $return .= '}';
    }
    // Insertion du résultat dans un tableau JSON
    $return ='{"reponse":['.$return.']}';
    // Retourne le résultat
    return $return;
  }

}
?>

'use strict';

// Définition des fonctions globales
app.controller('main', function ($scope, $utilisateur, $categorie, $mdSidenav, $mdToast) {
  // Variables globales
  $scope.document_titre = 'TP AngularJS';
  $scope.document_connexion = 'Se connecter';
  $scope.meta_description = 'TP AngularJS pour s\'entrainer';
  $scope.meta_auteur = 'Leslie Petrimaux';
  $scope.version = 'version 1.0';
  $scope.limit = '10';
  $scope.plus = '5';
  $scope.chargerplus = 'charger plus d\'éléments';
  $scope.action_retour = 'revenir à la page précédente';
  $scope.action_ouvrir = 'Ouvrir';
  $scope.action_modifier = 'Modifier';
  $scope.action_supprimer = 'Supprimer';
  $scope.action_enregistrer = 'Enregistrer';
  $scope.action_annuler = 'Annuler';
  // Variables des menus
  $scope.menu_deconnexion = 'Se déconnecter';
  $scope.menu_accueil = 'Accueil';
  $scope.menu_categorie = 'Catégories';
  $scope.menu_subhead_utilisateurs = 'Utilisateurs & droits';
  $scope.menu_utilisateurs_liste = 'Afficher la liste';
  $scope.menu_utilisateurs_droits = 'Éditer les droits';
  $scope.menu_subhead_categories = 'Catégories d\'articles';
  // Fonctions des menus
  $scope.toggleLeft = buildToggler('left');
  $scope.toggleRight = buildToggler('right');
  function buildToggler(componentId) {
    return function() { $mdSidenav(componentId).toggle(); };
  }
  // Fonctions pour la liste categories dans le menu
  $scope.menu_categories = null;
  $categorie.liste().then(function(d) { $scope.menu_categories = d; });
  // Navigation à partir du menu dans le site
  $scope.navigateTo = function (page) {
    window.location.href = '/#!/' + page;
    $mdSidenav('left').toggle();
  };
  // Navigation dans le site
  $scope.navigate = function (page) {
    window.location.href = '/#!/' + page;
  };
  // Retour à la page précédente
  $scope.retour = function() {
    window.history.back();
  };
  // Fonction de déconnexion
  $scope.deconnexion = function () {
    console.log('deco ?');
    $utilisateur.deconnexion().then(function(d) {
      if (d===true) { window.location.href = '/'; } else { $scope.makeToast(d); }
    });
  };
  // Définit le sens des tooltips
  $scope.tooltipDir = {
    top: 'top',
    bottom: 'bottom',
    left: 'left',
    right: 'right'
  };
	// Fonction de génération d'un Toast
	$scope.makeToast = function (msg) {
		var pinTo = $scope.getToastPosition();
		$mdToast.show(
			$mdToast.simple()
				.textContent(msg)
				.position(pinTo)
				.hideDelay(1000)
		);
	};
	// Gestion des Toasts
	var last = {
		bottom: false,
		top: true,
		left: false,
		right: true
	};
	$scope.toastPosition = angular.extend({},last);
	$scope.getToastPosition = function() {
		sanitizePosition();
		return Object.keys($scope.toastPosition)
			.filter(function(pos) { return $scope.toastPosition[pos]; })
			.join(' ');
	};
	function sanitizePosition() {
		var current = $scope.toastPosition;
		if ( current.bottom && last.top ) current.top = false;
		if ( current.top && last.bottom ) current.bottom = false;
		if ( current.right && last.left ) current.left = false;
		if ( current.left && last.right ) current.right = false;
		last = angular.extend({},current);
	}
});

// Définition des fonctions pour la page de connexion
app.controller('connexion', function($scope, $utilisateur, $mdDialog, $mdToast) {
  // Variables globales
  $scope.titre = 'Identifiez-vous';
  $scope.identifiant = 'Votre identifiant';
  $scope.motdepasse = 'Votre mot de passe';
  $scope.inscription_label = 's\'inscrire';
  $scope.connexion_label = 'se connecter';
  $scope.connexion = function () {
    $utilisateur.connexion(this.user.login, this.user.pass).then(function(d) {
      if (d===true) { location.reload(); } else { $scope.makeToast(d); }
    });
  };
  $scope.oublieMotDePasse = function(ev) {
    var confirm = $mdDialog.prompt()
    .title('Merci de renseigner votre adresse e-mail.')
    .placeholder('Adresse e-mail @axeconseils.com')
    .targetEvent(ev)
    .ok('envoyer mon mot de passe')
    .cancel('annuler');
    //When validation !
    $mdDialog.show(confirm).then(function(email) {
      $utilisateur.oublie(email).then(function(d) {
        if (d===true) { $scope.makeToast('Votre mot de passe a été envoyé avec succès !'); } else { $scope.makeToast(d); }
      });
    }, function() {
    });
  };
});

// Définition des fonctions pour la page d'accueil
app.controller('accueil', function($scope, $webapp) {
  // Variables pour la barre de navigation
  $scope.navigation = [
    {nom:'accueil',lien:'accueil', current:true}
  ];
  // Variables globales
  $scope.variable = 'page d\'accueil';
  // Liens vers les différentes zones du site
  $scope.pages = null;
  $webapp.liste().then(function(d) { $scope.pages = d; });
});

'use strict';

// Définition des fonctions pour la page d'accueil des categories
app.controller('categories', function($scope, $categorie, $routeParams, $mdDialog) {
  // Variables pour la barre de navigation
  $scope.navigation = [
    {nom:'accueil',lien:'accueil'},
    {nom:'catégories',lien:'categories', current:true}
  ];
  // Variables globales
  $scope.variable = 'page des categories';
  $scope.aria_label_ajouter_categorie = 'Ajouter une catégorie';
  // Fonctions pour la liste categories
  listeCategorie();
  function listeCategorie() {
    $scope.categories = null;
    $categorie.liste().then(function(d) { $scope.categories = d; });
  }
  // Fonction d'édition d'une catégorie
	$scope.editer = function(ev, categorie) {
		$mdDialog.show({
			controller: EditDialogController,
			templateUrl: 'templates/details-categorie.tmpl.php',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
			locals: { item: categorie }
		})
		.then(function() {
			// Sauvegarde
		}, function() {
			// Action annulée
		});
	};
	function EditDialogController($scope, $categorie, $mdDialog, $mdToast, item) {
    $scope.categorie = item;
		$scope.popup_titre_page = 'Création de la catégorie : ' + item.nom;
    $scope.popup_nom = 'Nom de la catégorie';
    $scope.popup_tag = 'Mot clé de la catégorie';
    $scope.action_enregistrer = 'Enregistrer';
    $scope.action_annuler = 'Annuler';
		$scope.hide = function() {
			$mdDialog.hide();
		};
		$scope.cancel = function() {
			$mdDialog.cancel();
		};
		$scope.save = function() {
      $categorie.editer(item.id, this.categorie.nom, this.categorie.tag).then(function(d) {
        if (d===true) {
          $scope.makeToast('La catégorie a été modifiée avec succès !');
		      listeCategorie();
		      $mdDialog.hide();
        } else {
          $scope.makeToast(d);
        }
      });
		};
  	// Fonction de génération d'un Toast
  	$scope.makeToast = function (msg) {
  		var pinTo = $scope.getToastPosition();
  		$mdToast.show(
  			$mdToast.simple()
  				.textContent(msg)
  				.position(pinTo)
  				.hideDelay(1000)
  		);
  	};
  	// Gestion des Toasts
  	var last = {
  		bottom: false,
  		top: true,
  		left: false,
  		right: true
  	};
  	$scope.toastPosition = angular.extend({},last);
  	$scope.getToastPosition = function() {
  		sanitizePosition();
  		return Object.keys($scope.toastPosition)
  			.filter(function(pos) { return $scope.toastPosition[pos]; })
  			.join(' ');
  	};
  	function sanitizePosition() {
  		var current = $scope.toastPosition;
  		if ( current.bottom && last.top ) current.top = false;
  		if ( current.top && last.bottom ) current.bottom = false;
  		if ( current.right && last.left ) current.left = false;
  		if ( current.left && last.right ) current.right = false;
  		last = angular.extend({},current);
  	}
	}
  // Foncgtion de suppression d'une catégorie
	$scope.supprimer = function(ev, id) {
		var confirm = $mdDialog.confirm()
					.title('Voulez-vous vraiment supprimer cette catégorie ?')
					.textContent('Si vous supprimez la catégorie, celle-ci sera effacé définitivement.')
					.ariaLabel('Supprimer catégorie')
					.targetEvent(ev)
					.ok('Supprimer')
					.cancel('Annuler');
		$mdDialog.show(confirm).then(function() {
      // Suppression
		}, function() {
			// Action annulée
		});
	};
});

// Définition des fonctions pour la page d'articles des categories
app.controller('categories-liste', function($scope, $categorie, $article, $routeParams, $rootScope) {
  // Fonctions pour la récupérer les informations de la catégorie
  $scope.categorie = null;
  $categorie.afficher($routeParams.tag).then(function(d) {
    // Vérifie que la réponse n'est pas nulle
    if (d!=null) {
      // Stocke les informations sur la catégorie
      $scope.categorie = d;
      // Variables pour la barre de navigation
      $scope.navigation = [
        {nom:'accueil',lien:'accueil'},
        {nom:'catégories',lien:'categories'},
        {nom:$scope.categorie[0].nom,lien:'categories/' + $scope.categorie[0].tag, current:true}
      ];
      // Variables globales
      $scope.variable = 'page des articles d\'une categorie';
      $scope.aria_label_ajouter_article = 'Ajouter un article';
      $scope.titre_page = 'Articles dans la catégorie : "' + $scope.categorie[0].nom + '"';
      $rootScope.title = $scope.titre_page;
      $scope.subhead_page = 'Retrouvez la liste complète des articles de la catégorie en cours';
      $scope.tag_categorie = $scope.categorie[0].tag;
      $scope.img_categorie = $scope.categorie[0].image;
      // Fonctions pour la liste des articles de la catégorie
      $scope.articles = null;
      $article.categorie($routeParams.tag).then(function(a) {
        if (a!='') { $scope.articles = a; } else { $scope.empty = 'Aucun article n\'est disponible pour cette catégorie.'; }
      });
    } else {
      // Retour à la page des catégories si la réponse est nulle
      window.location.href = '/#!/categories';
    }
  });
});

// Définition des fonctions pour la page d'articles des categories
app.controller('categories-article', function($scope, $article, $routeParams, $rootScope) {
  // Fonctions pour la récupérer les informations de la catégorie
  $scope.article = null;
  $article.afficher($routeParams.id).then(function(d) {
    // Vérifie que la réponse n'est pas nulle
    if (d!=null) {
      // Stocke les informations sur l'article
      $scope.article = d;
      // Variables pour la barre de navigation
      $scope.navigation = [
        {nom:'accueil',lien:'accueil'},
        {nom:'catégories',lien:'categories'},
        {nom:$scope.article[0].nom_categorie,lien:'categories/' + $scope.article[0].tag_categorie},
        {nom:$scope.article[0].nom,lien:'article-' + $scope.article[0].id, current:true}
      ];
      // Variables globales
      $scope.variable = 'page des articles d\'une categorie';
      $scope.auteur = 'Auteur :';
      $rootScope.title = '"' + $scope.article[0].nom + '" dans la catégorie : "' + $scope.article[0].nom_categorie + '"';
    } else {
      // Retour à la page d'accueil si la réponse est nulle
      window.location.href = '/';
    }
  });
});

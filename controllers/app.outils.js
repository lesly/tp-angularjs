'use strict';

// Définition des fonctions pour la page de recherche d'articles
app.controller('recherche-articles', function($scope, $recherche, $timeout, $q) {
  // Variables globales
  $scope.titre = 'Rechercher des articles';
  // Gestion de l'auto-complete pour les utilisateurs
  $scope.recherche = function(search) {
    var deferred = $q.defer();
    $timeout(function() {
        deferred.resolve($recherche.articles(search));
    }, Math.random() * 500, false);
    return deferred.promise;
  };
});

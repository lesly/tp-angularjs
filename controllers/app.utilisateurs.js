'use strict';

// Définition des fonctions pour la page d'accueil
app.controller('utilisateurs-liste', function($scope, $mdDialog, $utilisateur) {
  // Variables pour la barre de navigation
  $scope.navigation = [
    {nom:'accueil',lien:'accueil'},
    {nom:'liste des utilisateurs',lien:'utilisateurs/liste', current:true}
  ];
  // Variables globales
  $scope.titre_page = 'Afficher les informations des utilisateurs';
  $scope.subhead_page = 'Vous pouvez ajouter, éditer, afficher et supprimer des utilisateurs à partir de cette page';
  $scope.aria_label_ajouter_utilisateur = 'Ajouter un utilisateur';
  // Fonctions pour la liste utilisateurs
  $scope.utilisateurs = null;
  $utilisateur.liste().then(function(d) { $scope.utilisateurs = d; });
});

// Définition des fonctions pour la page d'inscription des utilisateurs
app.controller('utilisateurs-inscription', function($scope, $utilisateur) {
  // Variables globales
  $scope.document_connexion = 'S\'inscrire';
  $scope.titre = 'Inscrivez-vous';
  $scope.identifiant = 'Votre identifiant';
  $scope.motdepasse = 'Votre mot de passe';
  $scope.email = 'Votre adresse e-mail';
  $scope.nom = 'Votre nom';
  $scope.prenom = 'Votre prénom';
  $scope.inscription_label = 'enregistrer';
  // Fonction d'inscription
  $scope.inscription = function () {
    var login = this.user.login;
    var pass = this.user.pass;
    $utilisateur.inscription(this.user.login, this.user.pass, this.user.nom, this.user.prenom, this.user.email).then(function(d) {
      if (d===true) {
        $utilisateur.connexion(login, pass).then(function(etat) { if (etat===true) { window.location.href = '/'; } else { $scope.makeToast(etat); } });
      } else {
        $scope.makeToast(d);
      }
    });
  };
});

// Définition des fonctions pour la page des droits utilisateurs
app.controller('utilisateurs-droits', function($scope) {
  // Variables pour la barre de navigation
  $scope.navigation = [
    {nom:'accueil',lien:'accueil'},
    {nom:'droits des utilisateurs',lien:'utilisateurs/droits', current:true}
  ];
  // Variables globales
  $scope.variable = 'page des droits utilisateurs';
});

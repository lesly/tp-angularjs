'use strict';

// Définition de l'application du site
var app = angular.module( 'Angular', [ 'ngRoute', 'ngMaterial' ] );

// Définitions des couleurs pour le site
app.config(["$routeProvider", "$mdThemingProvider", function ($routeProvider, $mdThemingProvider) {
  // Définit les couleurs personnalisées
  var colorDark = $mdThemingProvider.extendPalette('blue', {
    '500': '#0d47a1',
    'contrastDefaultColor': 'light'
  });
  var colorLight = $mdThemingProvider.extendPalette('orange', {
    '500': '#ffab00',
    'contrastDefaultColor': 'light'
  });
  // Enregistre les couleurs personnalisées
  $mdThemingProvider.definePalette('colorPrimary', colorDark);
  $mdThemingProvider.definePalette('colorAccent', colorLight);
  // Définit le thème du site
  $mdThemingProvider.theme('default')
    .primaryPalette('colorPrimary')
    .accentPalette('colorAccent')
    .warnPalette('red')
    .backgroundPalette('grey');
  $mdThemingProvider.setDefaultTheme('default');
  $mdThemingProvider.alwaysWatchTheme(true);
}]);

// Définitions des titres en fonction des routes utilisées
app.run(['$rootScope', function($rootScope) {
  $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
    if (!current.$$route) {
      current.$$route = 'Erreur';
      current.$$route.title = 'Erreur';
    }
    $rootScope.title = current.$$route.title;
  });
}]);

'use strict';

// Directives génériques
app.directive("mdRechercheArticles", function() {return { restrict: "E", templateUrl: "templates/recherche-articles.tmpl.php", controller: 'recherche-articles' }});
app.directive("mdCardRetour", function() {return { restrict: "E", templateUrl: "templates/card-retour.tmpl.php" }});


// Directives pour les articles
app.directive("mdCardListeArticles", function() {return { restrict: "E", templateUrl: "templates/card-liste-article.tmpl.php" }});
app.directive("mdCardArticle", function() {return { restrict: "E", templateUrl: "templates/card-article.tmpl.php" }});
app.directive("mdInfosArticle", function() {return { restrict: "E", templateUrl: "templates/infos-article.tmpl.php" }});

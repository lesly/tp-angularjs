'use strict';

// Définitions des "routes" / pages pour le site
app.config(["$routeProvider", function ($routeProvider) {
    $routeProvider
    // PAGE D'ACCUEIL
    .when('/accueil', {
      title: 'Accueil',
      templateUrl: 'views/accueil.php',
      controller: 'accueil'
    })
    // PAGE D'INSCRIPTION
    .when('/inscription', {
      title: 'Créez votre compte',
      templateUrl: 'views/inscription.php',
      controller: 'utilisateurs-inscription'
    })
    // PAGE DE LISTE DES UTILISATEURS
    .when('/utilisateurs/liste', {
      title: 'Afficher la liste des utilisateurs',
      templateUrl: 'views/utilisateurs/liste.php',
      controller: 'utilisateurs-liste'
    })
    // PAGE D'EDITION DES DROITS UTILISATEURS
    .when('/utilisateurs/droits', {
      title: 'Édition des droits utilisateurs',
      templateUrl: 'views/utilisateurs/droits.php',
      controller: 'utilisateurs-droits'
    })
    // PAGE D'ACCUEIL DES CATEGORIES
		.when('/categories', {
			title: 'Catégories d\'articles',
      templateUrl: 'views/categories/accueil.php',
			controller: 'categories'
		})
    // PAGE DE LISTE D'ARTICLES D'UNE CATEGORIE
		.when('/categories/:tag', {
      templateUrl: 'views/categories/liste.php',
			controller: 'categories-liste'
		})
    // PAGE D'UN ARTICLE
		.when('/categories/:tag/article-:id', {
      templateUrl: 'views/categories/article.php',
			controller: 'categories-article'
		})
    // SINON
    .otherwise({
      redirectTo: '/accueil'
    });
  }
]);

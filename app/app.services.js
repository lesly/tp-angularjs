'use strict';

// Services de manipulations des articles
app.factory('$webapp', function($http) {
  var $webapp = {
    // Liste complète des sections du site
    liste: function() {
      return $http.get("models/liste.php?type=accueil").then(function (response) {
        return response.data.reponse;
      });
    },
  };
  return $webapp;
});

// Services de manipulations des utilisateurs
app.factory('$utilisateur', function($http) {
  var $utilisateur = {
    // Connexion utilisateur
    connexion: function(login, pass) {
      var data = "login=" + encodeURIComponent(login) + "&"
      + "pass=" + encodeURIComponent(pass);
      console.log('models/connexion.php?' + data);
      return $http({
        method: 'POST',
        url: 'models/connexion.php',
        data: data,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function(response) {
        console.log(response.data.reponse);
        if (response.data.reponse==='correct') { return true; } else { return response.data.respone; }
      });
    },
    // Liste complète des utilisateur
    deconnexion: function() {
      return $http.get("models/deconnexion.php").then(function (response) {
        if (response.data.reponse==='correct') { return true; } else { return response.data.respone; }
      });
    },
    // Renvoi de mot de passe
    oublie: function(email) {
      var data = 'email=' + email;
      return $http({
        method: 'POST',
        url: 'models/oublie.php?action=password',
        data: data,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function(response) {
        if (response.data.reponse==='correct') { return true; } else { return response.data.respone; }
      });
    },
    // Inscription nouvel utilisateur
    inscription: function(login, pass, nom, prenom, email) {
      var data = "login=" + encodeURIComponent(login)
      + "&pass=" + encodeURIComponent(pass)
      + "&nom=" + encodeURIComponent(nom)
      + "&prenom=" + encodeURIComponent(prenom)
      + "&email=" + encodeURIComponent(email);
      return $http({
        method: 'POST',
        url: 'models/ajouter.php?type=utilisateur',
        data: data,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function(response) {
        if (response.data.reponse==='correct') { return true; } else { return response.data.respone; }
      });
    },
    // Liste complète des utilisateur
    liste: function() {
      return $http.get("models/liste.php?type=utilisateurs&all=true").then(function (response) {
        return response.data.reponse;
      });
    },
  };
  return $utilisateur;
});

// Services de manipulations des catégories
app.factory('$categorie', function($http) {
  var $categorie = {
    // Affiche une catégorie
    afficher: function(categorie) {
      return $http.get("models/afficher.php?type=categories&tag=" + categorie).then(function (response) {
        if (response.data.reponse!='error') { return response.data.reponse; } else { return null; }
      });
    },
    // Liste complète des catégories
    liste: function() {
      return $http.get("models/liste.php?type=categories&all=true").then(function (response) {
        return response.data.reponse;
      });
    },
    // Editer une catégorie
    editer: function(id, nom, tag) {
      var data = "id=" + id
      + "&nom=" + encodeURIComponent(nom)
      + "&tag=" + encodeURIComponent(tag);
      console.log('models/editer.php?type=categories&' + data);
      return $http({
        method: 'POST',
        url: 'models/editer.php?type=categories',
        data: data,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .then(function(response) {
        if (response.data.reponse==='correct') { return true; } else { return response.data.respone; }
      });
    },
  };
  return $categorie;
});

// Services de manipulations des articles
app.factory('$article', function($http) {
  var $article = {
    // Affiche un article
    afficher: function(id) {
      return $http.get("models/afficher.php?type=articles&id=" + id).then(function (response) {
        if (response.data.reponse!='error') { return response.data.reponse; } else { return null; }
      });
    },
    // Liste complète des articles
    liste: function() {
      return $http.get("models/liste.php?type=articles&all=true").then(function (response) {
        return response.data.reponse;
      });
    },
    // Liste des articles d'une catégorie
    categorie: function(categorie) {
      return $http.get("models/liste.php?type=articles&tag=" + categorie).then(function (response) {
        return response.data.reponse;
      });
    },
  };
  return $article;
});

// Services de manipulations des recherches
app.factory('$recherche', function($http) {
  var $recherche = {
    // Liste des articles pour les recherches
    articles: function(q) {
      return $http.get("models/recherche.php?type=articles&q=" + q)
      .then(function (response) {
        return response.data.reponse;
      });
    },
  };
  return $recherche;
});

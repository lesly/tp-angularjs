<?php
// Auto-loader
require_once('../classes/config.inc.php');
// Autorise l'accès à partir de n'importe quelle origine
header("Access-Control-Allow-Origin: *");
// Définit la page comme étant un tableau JSON
header("Content-Type: application/json; charset=UTF-8");
// Charge la classe Utilisateur
$utilisateurs = new Utilisateurs();
// Si vide, définit à nul
if (!empty($_REQUEST['login'])) { $login = $_REQUEST['login']; } else { $login = null; }
if (!empty($_REQUEST['pass'])) { $pass = $_REQUEST['pass']; } else { $pass = null; }
// Tableau de données
$tableau = [
	'login' => $login,
	'pass' => $pass
];
// Retourne le résultat
echo $utilisateurs->connexion($tableau);
?>

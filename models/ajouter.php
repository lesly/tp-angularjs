<?php
// Auto-loader
require_once('../classes/config.inc.php');
// Autorise l'accès à partir de n'importe quelle origine
header("Access-Control-Allow-Origin: *");
// Définit la page comme étant un tableau JSON
header("Content-Type: application/json; charset=UTF-8");
// Vérifie si les variables sont vides
if (!empty($_REQUEST['login'])) { $login = $_REQUEST['login']; } else { $login = null; }
if (!empty($_REQUEST['pass'])) { $pass = $_REQUEST['pass']; } else { $pass = null; }
if (!empty($_REQUEST['email'])) { $email = $_REQUEST['email']; } else { $email = null; }
if (!empty($_REQUEST['nom'])) { $nom = $_REQUEST['nom']; } else { $nom = null; }
if (!empty($_REQUEST['prenom'])) { $prenom = $_REQUEST['prenom']; } else { $prenom = null; }
// Vérifie le type de liste à charger
switch ($_REQUEST['type']) {
	case 'accueil':
		// Charge la classe Webapp
		$liste = new Webapp();
		break;
	case 'articles':
		// Charge la classe Articles
		$liste = new Articles();
		break;
	case 'categories':
		// Charge la classe Categories
		$liste = new Categories();
		break;
	default:
		// Charge la classe Utilisateurs
		$liste = new Utilisateurs();
		break;
}
// Tableau de données
$tableau = [
	'login' => $login,
	'pass' => $pass,
	'email' => $email,
	'nom' => $nom,
	'prenom' => $prenom
];
// Retourne le résultat
echo $liste->ajouter($tableau);
?>

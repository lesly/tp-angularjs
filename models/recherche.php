<?php
// Auto-loader
require_once('../classes/config.inc.php');
// Autorise l'accès à partir de n'importe quelle origine
header("Access-Control-Allow-Origin: *");
// Définit la page comme étant un tableau JSON
header("Content-Type: application/json; charset=UTF-8");
// Vérifie si les variables sont vides
if (!empty($_REQUEST['q'])) { $q = $_REQUEST['q']; } else { $q = null; }
// Vérifie le type de liste à charger
switch ($_REQUEST['type']) {
	case 'accueil':
		// Charge la classe Webapp
		$liste = new Webapp();
		break;
	case 'articles':
		// Charge la classe Articles
		$liste = new Articles();
		break;
	case 'categories':
		// Charge la classe Categories
		$liste = new Categories();
		break;
	default:
		// Charge la classe Utilisateurs
		$liste = new Utilisateurs();
		break;
}
// Tableau de données
$tableau = [
	'q' => $q
];
// Retourne le résultat
echo $liste->recherche($tableau);
?>

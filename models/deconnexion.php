<?php
// Auto-loader
require_once('../classes/config.inc.php');
// Autorise l'accès à partir de n'importe quelle origine
header("Access-Control-Allow-Origin: *");
// Définit la page comme étant un tableau JSON
header("Content-Type: application/json; charset=UTF-8");
// Charge la classe Utilisateur
$utilisateurs = new Utilisateurs();
// Retourne le résultat
echo $utilisateurs->deconnexion();
?>

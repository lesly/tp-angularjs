<?php
// Auto-loader
require_once('../classes/config.inc.php');
// Autorise l'accès à partir de n'importe quelle origine
header("Access-Control-Allow-Origin: *");
// Définit la page comme étant un tableau JSON
header("Content-Type: application/json; charset=UTF-8");
// Vérifie si les variables sont vides
if (!empty($_REQUEST['tag'])) { $tag = $_REQUEST['tag']; } else { $tag = null; }
if (!empty($_REQUEST['id'])) { $id = $_REQUEST['id']; } else { $id = null; }
// Vérifie le type de liste à charger
switch ($_REQUEST['type']) {
	case 'articles':
		// Charge la classe Articles
		$afficher = new Articles();
		break;
	case 'categories':
		// Charge la classe Categories
		$afficher = new Categories();
		break;
	default:
		// Charge la classe Utilisateurs
		$afficher = new Utilisateurs();
		break;
}
// Tableau de données
$tableau = [
	'tag' => $tag,
	'id' => $id
];
// Retourne le résultat
echo $afficher->afficher($tableau);
?>

<?php require_once("./classes/config.inc.php"); ?>
<!DOCTYPE html>
<html ng-app="Angular" ng-controller="main">
  <head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="description" content="{{meta_description}}">
    <meta name="author" content="{{meta_auteur}}">
    <meta name="robots" content="noindex, nofollow">
    <!-- Icones et Titre Navigateur -->
    <link rel="icon" href="/images/favicon.ico" />
    <title>{{document_titre}}</title>
    <!-- CSS -->
    <!-- <link href='//fonts.googleapis.com/css?family=Noto%20Sans:400,300,500,700' rel='stylesheet' type='text/css' media="screen"> -->
		<link href='/css/fontGoogle.css' rel='stylesheet' type='text/css' media="screen">
    <link rel="stylesheet" href="/bower_components/angular-material/angular-material.min.css" media="screen">
    <link rel="stylesheet" href="/css/style.css" media="screen">
    <!--[if lte IE 8]
      <script>
        document.createElement('ng-view');
        // Other custom elements
      </script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui/0.4.0/angular-ui-ieshiv.js"></script>
    <![endif]-->
  </head>
  <body>
<?php if (!empty($_SESSION['login'])) { ?>
    <!-- SIDENAV -->
    <md-sidenav class="md-sidenav-left" md-component-id="left" md-whiteframe="4">
      <md-toolbar ng-click="toggleLeft()" class="md-primary md-toolbar-tools">
        <md-button class="md-icon-button" aria-label="Menu">
          <md-icon md-svg-src="/images/ic_arrow_back_white_48px.svg"></md-icon>
        </md-button>
        <h1><?php echo $_SESSION['prenom'].' '.$_SESSION['nom']; ?></h1>
      </md-toolbar>
      <md-content>
        <span flex></span>
        <md-divider></md-divider>
        <md-list-item ng-click="navigateTo('accueil')"><md-icon md-svg-src="/images/ic_home_48px.svg"></md-icon> <p>{{menu_accueil}}</p></md-list-item>
        <md-list-item ng-click="navigateTo('categories')"><md-icon md-svg-src="/images/ic_chrome_reader_mode_48px.svg"></md-icon> <p>{{menu_categorie}}</p></md-list-item>
        <md-divider></md-divider>
        <md-subheader class="md-no-sticky">{{menu_subhead_categories}}</md-subheader>
        <md-divider></md-divider>
        <md-list-item ng-click="navigateTo('categories/' + item.tag)" ng-repeat="item in menu_categories"><md-icon md-svg-src="/images/ic_align_left_48px.svg"></md-icon> <p>{{item.nom}}</p></md-list-item>
        <md-divider></md-divider>
<?php if ($_SESSION['role']!='U') { ?>
        <md-subheader class="md-no-sticky">{{menu_subhead_utilisateurs}}</md-subheader>
        <md-divider></md-divider>
        <md-list-item ng-click="navigateTo('utilisateurs/liste')"><md-icon md-svg-src="/images/ic_list_48px.svg"></md-icon> <p>{{menu_utilisateurs_liste}}</p></md-list-item>
        <md-list-item ng-click="navigateTo('utilisateurs/droits')"><md-icon md-svg-src="/images/ic_lock_open_48px.svg"></md-icon> <p>{{menu_utilisateurs_droits}}</p></md-list-item>
        <md-divider></md-divider>
<?php } ?>
      </md-content>
    </md-sidenav>
<?php } ?>
    <!-- TOOLBAR -->
    <md-toolbar class="md-primary">
      <div class="md-toolbar-tools">
<?php if (!empty($_SESSION['login'])) { ?>
        <md-button class="md-icon-button" aria-label="Sidenav" ng-click="toggleLeft()">
          <md-icon md-svg-icon="images/ic_menu_white_48px.svg"></md-icon>
        </md-button>
        <h2>{{title}}</h2>
<?php } else { ?>
        <md-button class="md-icon-button" aria-label="Home">
          <md-icon md-svg-icon="images/ic_home_white_48px.svg"></md-icon>
        </md-button>
        <h2>{{document_connexion}}</h2>
<?php } ?>
        <span flex></span>
<?php if (!empty($_SESSION['login'])) { ?>
        <md-menu md-offset="0 -7">
          <md-button aria-label="Open logout menu" class="md-icon-button" ng-click="$mdMenu.open($event)">
            <md-icon md-svg-icon="images/ic_more_vert_white_48px.svg"></md-icon>
          </md-button>
          <md-menu-content width="4">
            <md-menu-item>
              <md-button ng-click="deconnexion()"> <span md-menu-align-target>{{menu_deconnexion}}</span> </md-button>
            </md-menu-item>
          </md-menu-content>
        </md-menu>
<?php } ?>
      </div>
    </md-toolbar>
    <!-- CONTENT -->
    <md-content ng-view></md-content>
    <!-- FOOTER -->
    <footer layout="column" layout-align="end center">
      <div>{{document_titre}} powered by <strong>{{meta_auteur}}</strong></div>
      <div><em>{{version}}</em></div>
    </footer>
    <!-- JAVASCRIPT /!\ ORDRE DE CHARGEMENT DES SCRIPTS ! /!\ -->
    <script src="/bower_components/angular/angular.js"></script>
    <script src="/bower_components/angular-route/angular-route.js"></script>
    <script src="/bower_components/angular-aria/angular-aria.js"></script>
    <script src="/bower_components/angular-animate/angular-animate.js"></script>
    <script src="/bower_components/angular-material/angular-material.js"></script>
    <script src="/bower_components/svg-assets-cache.js/svg-assets-cache.js"></script>
    <script src="/bower_components/masonry/dist/masonry.pkgd.js"></script>
    <script src="/app/app.module.js"></script>
    <script src="/app/app.routes.js"></script>
    <script src="/app/app.directives.js"></script>
    <script src="/app/app.services.js"></script>
    <script src="/controllers/app.main.js"></script>
    <script src="/controllers/app.outils.js"></script>
    <script src="/controllers/app.utilisateurs.js"></script>
    <script src="/controllers/app.categories.js"></script>
  </body>
</html>

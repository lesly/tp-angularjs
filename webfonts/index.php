<!DOCTYPE html>
<html lang="fr" ng-app="aceIntranet">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
		<meta name="description" content="Portail intranet Axe Conseils Expertise">
		<meta name="author" content="Leslie Petrimaux">
		<meta name="robots" content="noindex, nofollow">
		<!-- Icones et Titre Navigateur -->
		<link rel="icon" href="/img/favicon.png" />
		<title>Portail Intranet Axe Conseils Expertise</title>
		<!-- Web Fonts Dependencies -->
		<link href='//fonts.googleapis.com/css?family=Noto%20Sans:400,300,500,700' rel='stylesheet' type='text/css'>
		<link href='/css/fontGoogle.css' rel='stylesheet' type='text/css'>
		<!-- <link rel="stylesheet" href="/css/fontMaterialDesignIcons.css"> -->
		<!-- Calendar Dependencies -->
		<link rel="stylesheet" href="/css/angular-material-event-calendar.css">
		<!-- CSS Dependencies -->
		<link rel="stylesheet" href="/bower_components/angular-material/angular-material.min.css">
		<link rel="stylesheet" href="/css/global.css">
	</head>
	<body ng-controller="global" ng-cloak style="height: 100%;">
		<md-toolbar class="md-primary" md-scroll-shrink>
			<div class="md-toolbar-tools">
				<h2>Accès interdit !</h2>
				<span flex></span>
			</div>
		</md-toolbar>
    <p>&#160;</p>
    <p>&#160;</p>
		<section layout="row" flex ng-cloak>
			<md-content flex>
				<div class="section--center mdl-grid" ng-controller="connexion" align="center">
					<div flex="100" flex-gt-md="40" layout="column" layout-align="center">
						<form name="appLogin" ng-submit="connexion()">
							<md-card align="left">
							<img src="/img/logo_xlarge.png" alt="{{logo_alt}}">
								<md-card-header>
									<md-card-header-text layout-align="center center">
										<span class="md-headline">Vous n'avez pas accès à cette page. :-(</span>
										<a href="/" class="md-subhead">Cliquez ici pour revenir sur l'intranet.</a>
									</md-card-header-text>
								</md-card-header>
							</md-card>
						</form>
					</div>
				</div>
			</md-content>
		</section>
    <p>&#160;</p>
    <p>&#160;</p>
		<footer layout="column" layout-align="end center" class="md-copyright">
			<div>2017+ © Axe Conseils Expertise</div>
		</footer>
		<!-- AngularJS Dependencies -->
		<script src="/bower_components/angular/angular.min.js"></script>
		<script src="/bower_components/angular-i18n/angular-locale_fr-fr.js"></script>
		<script src="/bower_components/moment/moment.js"></script>
		<script src="/bower_components/angular-animate/angular-animate.min.js"></script>
		<script src="/bower_components/angular-route/angular-route.min.js"></script>
		<script src="/bower_components/angular-aria/angular-aria.min.js"></script>
		<script src="/bower_components/angular-messages/angular-messages.min.js"></script>
		<script src="/bower_components/angular-material/angular-material.min.js"></script>
		<script src="/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
		<script src="/bower_components/material-calendar/angular-material-calendar.js"></script>
		<script src="/bower_components/masonry/dist/masonry.pkgd.js"></script>
		<script src="/bower_components/angular-cookies/angular-cookies.min.js"></script>
		<script src="/js/svgAssetsCache.js"></script>
		<script src="/js/angular-material-event-calendar.js"></script>
		<!-- Angular App -->
		<script src="/app/app.module.js"></script>
		<!-- Controlleurs -->
		<script src="/controllers/global.js"></script>
		<script src="/controllers/mon-espace.js"></script>
		<script src="/controllers/mon-espace.support.js"></script>
		<script src="/controllers/mon-espace.conges-utilisateur.js"></script>
		<script src="/controllers/mon-espace.conges-actions.js"></script>
		<script src="/controllers/mon-espace.matrices-utilisateur.js"></script>
		<script src="/controllers/mon-espace.matrices-actions.js"></script>
		<script src="/controllers/gestion.js"></script>
		<script src="/controllers/gestion.support.js"></script>
		<script src="/controllers/gestion.collaborateurs.js"></script>
		<script src="/controllers/gestion.conges.js"></script>
		<!-- Global JS -->
	</body>
</html>

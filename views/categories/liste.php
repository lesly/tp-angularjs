<?php
require_once("./../../classes/config.inc.php");
if (empty($_SESSION['login'])) {
  require_once("./../connexion.php");
} else {
?>
<div layout="column" layout-gt-sm="row" layout-align="center" layout-wrap>
  <div flex="100">
    <md-card>
      <md-card-actions layout="row" layout-align="start center" layout-wrap>
        <md-button ng-repeat="item in navigation" ng-click="navigate(item.lien)" ng-disabled="{{ item.current ? 'true' : 'false' }}" md-no-ink class="md-primary">{{item.nom}}</md-button>
      </md-card-actions>
    </md-card>
  </div>
  <div flex="100" align="center">
    <md-recherche-articles></md-recherche-articles>
  </div>
  <div flex="100">
    <md-card-liste-articles></md-card-liste-articles>
  </div>
</div>
<md-button ng-click="ajouter($event)" class="md-fab" aria-label="{{aria_label_ajouter_article}}">
  <md-tooltip md-direction="{{tooltipDir.left}}">{{aria_label_ajouter_article}}</md-tooltip>
    <md-icon md-svg-src="images/ic_add_white_48px.svg"></md-icon>
</md-button>
<?php
}
?>

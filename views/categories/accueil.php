<?php
require_once("./../../classes/config.inc.php");
if (empty($_SESSION['login'])) {
  require_once("./../connexion.php");
} else {
?>
<div layout="column" layout-gt-sm="row" layout-align="center" layout-wrap>
  <div flex="100">
    <md-card>
      <md-card-actions layout="row" layout-align="start center" layout-wrap>
        <md-button ng-repeat="item in navigation" ng-click="navigate(item.lien)" ng-disabled="{{ item.current ? 'true' : 'false' }}" md-no-ink class="md-primary">{{item.nom}}</md-button>
      </md-card-actions>
    </md-card>
  </div>
  <div flex="100" align="center">
    <md-recherche-articles></md-recherche-articles>
  </div>
  <div flex="100" layout="row" layout-margin flex layout-align="space-around start" layout-wrap>
    <md-card ng-repeat="item in categories" flex="40" flex-gt-sm="20">
      <div flex layout="column" layout-align="center center">
        <md-card ng-click="navigate('categories/' + item.tag)" class="w150 round primary md-add-clic" layout="row" layout-align="center center">
          <md-card-actions>
            <md-icon md-svg-src="{{item.image}}" class="s96"></md-icon>
          </md-card-actions>
        </md-card>
      </div>
      <div flex layout="row" layout-align="space-between center" class="pad5dpLeft" hide show-gt-sm>
        <span class="fontPrimary fontUppercase fontBold">{{item.nom}}</span>
        <div>
          <md-menu md-offset="0 7">
            <md-button aria-label="Open logout menu" class="md-icon-button" ng-click="$mdMenu.open($event)">
              <md-icon md-svg-icon="images/ic_more_vert_48px.svg"></md-icon>
            </md-button>
            <md-menu-content width="4">
              <md-menu-item>
                <md-button ng-click="navigate('categories/' + item.tag)"> <md-icon md-svg-src="images/ic_open_in_new_48px.svg"></md-icon> <span md-menu-align-target>{{action_ouvrir}}</span> </md-button>
              </md-menu-item>
              <md-divider></md-divider>
              <md-menu-item <?php if ($_SESSION['role']=='U') { echo 'ng-hide="true"'; } ?>>
                <md-button ng-click="editer($event, item)"> <md-icon md-svg-src="images/ic_mode_edit_48px.svg"></md-icon> <span md-menu-align-target>{{action_modifier}}</span> </md-button>
              </md-menu-item>
              <md-menu-item <?php if ($_SESSION['role']!='A') { echo 'ng-hide="true"'; } ?>>
                <md-button ng-click="supprimer($event, item.id)"> <md-icon md-svg-src="images/ic_delete_48px.svg"></md-icon> <span md-menu-align-target>{{action_supprimer}}</span> </md-button>
              </md-menu-item>
            </md-menu-content>
          </md-menu>
        </div>
      </div>
    </md-card>
  </div>
</div>
<?php if ($_SESSION['role']!='U') { ?>
<md-button ng-click="ajouter($event)" class="md-fab" aria-label="{{aria_label_ajouter_categorie}}">
  <md-tooltip md-direction="{{tooltipDir.left}}">{{aria_label_ajouter_categorie}}</md-tooltip>
    <md-icon md-svg-src="images/ic_add_white_48px.svg"></md-icon>
</md-button>
<?php
}
}
?>

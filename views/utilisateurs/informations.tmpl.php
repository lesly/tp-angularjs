<md-dialog aria-label="Mango (Fruit)">
  <form ng-cloak ng-submit="sauvegarder()">
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>{{dialog_titre}}</h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">
          <md-icon md-svg-src="images/ic_close_white_48px.svg" aria-label="Fermer la fenêtre"></md-icon>
        </md-button>
      </div>
    </md-toolbar>
    <md-dialog-content>
      <div class="md-dialog-content">
        <h2>{{variable}}</h2>
      </div>
    </md-dialog-content>
    <md-dialog-actions layout="row" layout-align="space-between center">
      <md-button ng-click="cancel()" class="md-accent md-button-margin md-button-padding">{{action_annuler}}</md-button>
      <md-button type="submit" class="md-raised md-primary md-button-margin md-button-padding">{{action_sauvegarder}}</md-button>
    </md-dialog-actions>
  </form>
</md-dialog>

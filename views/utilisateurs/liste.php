<?php
require_once("./../../classes/config.inc.php");
if (empty($_SESSION['login'])) {
  require_once("./../connexion.php");
} else {
?>
<div layout="column" layout-gt-sm="row" layout-align="center" layout-wrap>
  <div flex="100">
    <md-card>
      <md-card-actions layout="row" layout-align="start center" layout-wrap>
        <md-button ng-repeat="item in navigation" ng-click="navigate(item.lien)" ng-disabled="{{ item.current ? 'true' : 'false' }}" md-no-ink class="md-primary">{{item.nom}}</md-button>
      </md-card-actions>
    </md-card>
  </div>
  <div flex="100">
    <md-card>
      <md-card-header>
        <md-card-avatar>
          <md-icon class="md-avatar-icon" md-svg-icon="images/ic_person_white_48px.svg"></md-icon>
        </md-card-avatar>
        <md-card-header-text>
          <span class="md-title">{{titre_page}}</span>
          <span class="md-subhead">{{subhead_page}}</span>
        </md-card-header-text>
      </md-card-header>
      <md-divider></md-divider>
      <md-list-item class="list-content md-2-line" ng-repeat="item in utilisateurs | limitTo: limit as results">
        <md-menu flex="5">
          <md-button class="md-icon-button">
            <md-icon md-svg-icon="images/ic_more_vert_48px.svg" hide show-gt-sm></md-icon>
          </md-button>
          <md-menu-content width="4">
            <md-menu-item>
              <md-button ng-click=""> <md-icon md-svg-src="images/ic_mode_edit_48px.svg"></md-icon> <span md-menu-align-target>{{action_modifier}}</span> </md-button>
            </md-menu-item>
            <md-menu-item <?php if ($_SESSION['role']!='A') { echo 'ng-hide="true"'; } ?>>
              <md-button ng-click=""> <md-icon md-svg-src="images/ic_delete_48px.svg"></md-icon> <span md-menu-align-target>{{action_supprimer}}</span> </md-button>
            </md-menu-item>
          </md-menu-content>
        </md-menu>
        <div flex="70" flex-gt-sm="85" class="md-list-item-text">
          <h3>{{item.prenom + ' ' + item.nom}} <span class="font07em fontBlack">({{item.login}})</span></h3>
          <p class="font08em fontBlack" hide show-gt-sm>{{item.email}}</p>
        </div>
        <p flex="30" flex-gt-sm="20"><md-button class="{{item.class}}">{{item.role}}</md-button></p>
      </md-list-item>
      <md-list-item ng-hide="results.length >= utilisateurs.length" class="noright" layout-align="center">
        <md-button class="md-block md-primary" ng-click="limit = limit + plus" aria-label="{{chargerplus}}" flex>{{chargerplus}}</md-button>
      </md-list-item>
      <md-card-actions layout="column" layout-gt-sm="row" layout-align="end center">
        <md-button ng-click="retour()" md-no-ink class="md-primary md-button-padding">{{action_retour}}</md-button>
      </md-card-actions>
    </md-card>
  </div>
</div>
<?php if ($_SESSION['role']=='A') { ?>
<md-button ng-click="ajouter($event)" class="md-fab" aria-label="{{aria_label_ajouter_utilisateur}}">
  <md-tooltip md-direction="{{tooltipDir.left}}">{{aria_label_ajouter_utilisateur}}</md-tooltip>
    <md-icon md-svg-src="images/ic_add_white_48px.svg"></md-icon>
</md-button>
<?php
}
}
?>

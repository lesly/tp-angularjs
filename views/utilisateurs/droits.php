<?php
require_once("./../../classes/config.inc.php");
if (empty($_SESSION['login'])) {
  require_once("./../connexion.php");
} else {
?>
<div layout="column" layout-gt-sm="row" layout-align="center" layout-wrap>
  <div flex="100">
    <md-card>
      <md-card-actions layout="row" layout-align="start center" layout-wrap>
        <md-button ng-repeat="item in navigation" ng-click="navigate(item.lien)" ng-disabled="{{ item.current ? 'true' : 'false' }}" md-no-ink class="md-primary">{{item.nom}}</md-button>
      </md-card-actions>
    </md-card>
  </div>
  {{variable}}
</div>
<?php
}
?>

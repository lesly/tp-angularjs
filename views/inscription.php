<div class="section--center mdl-grid" align="center">
  <div flex="100" flex-gt-md="40" layout="column" layout-align="center">
    <form name="appInscription" ng-submit="inscription()">
      <md-card align="left">
        <md-card-header>
          <md-card-header-text>
            <span class="md-headline">{{titre}}</span>
          </md-card-header-text>
        </md-card-header>
        <md-card-title>
          <md-card-title-text>
            <md-input-container class="md-block">
              <label>{{identifiant}}</label>
              <input type="text" ng-model="user.login" required>
            </md-input-container>
            <md-input-container class="md-block">
              <label>{{motdepasse}}</label>
              <input type="password" ng-model="user.pass" required>
            </md-input-container>
            <md-input-container class="md-block">
              <label>{{nom}}</label>
              <input type="text" ng-model="user.nom" required>
            </md-input-container>
            <md-input-container class="md-block">
              <label>{{prenom}}</label>
              <input type="text" ng-model="user.prenom" required>
            </md-input-container>
            <md-input-container class="md-block">
              <label>{{email}}</label>
              <input type="mail" ng-model="user.email" required>
            </md-input-container>
            <div class="alert" role="alert" ng-show="errorInscription">{{errorInscription}}</div>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="end center">
          <md-button type="submit" class="md-raised md-primary md-button-margin md-button-padding">{{inscription_label}}</md-button>
        </md-card-actions>
      </md-card>
    </form>
  </div>
</div>

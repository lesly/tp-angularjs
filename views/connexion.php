<div class="section--center mdl-grid" ng-controller="connexion" align="center">
  <div flex="100" flex-gt-md="40" layout="column" layout-align="center">
    <form name="appLogin" ng-submit="connexion()">
      <md-card align="left">
        <md-card-header>
          <md-card-header-text>
            <span class="md-headline">{{titre}}</span>
          </md-card-header-text>
        </md-card-header>
        <md-card-title>
          <md-card-title-text>
            <md-input-container class="md-block">
              <label>{{identifiant}}</label>
              <input type="text" ng-model="user.login" required>
            </md-input-container>
            <md-input-container class="md-block">
              <label>{{motdepasse}}</label>
              <input type="password" ng-model="user.pass" required>
            </md-input-container>
            <div class="alert" role="alert" ng-show="errorLogin">{{errorLogin}}</div>
          </md-card-title-text>
        </md-card-title>
        <md-card-actions layout="row" layout-align="space-between center">
          <md-button ng-click="navigate('inscription')" class="md-accent md-button-margin md-button-padding">{{inscription_label}}</md-button>
          <md-button type="submit" class="md-raised md-primary md-button-margin md-button-padding">{{connexion_label}}</md-button>
        </md-card-actions>
      </md-card>
    </form>
  </div>
</div>
